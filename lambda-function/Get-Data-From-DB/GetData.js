const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const { DynamoDBDocumentClient, ScanCommand } = require("@aws-sdk/lib-dynamodb");

const ddbClient = new DynamoDBClient({ region: "us-east-1" });
const ddbDocClient = DynamoDBDocumentClient.from(ddbClient);

async function fetchMessages() {
    const params = {
        TableName: 'TABLENAME', // Hier name der DyanmoDB Tabelle eingeben und TABLENAME erstetzen
    };

    try {
        const data = await ddbDocClient.send(new ScanCommand(params));
        return data.Items;
    } catch (err) {
        console.error(err);
        throw err;
    }
}

exports.handler = async (event) => {
    try {
        const messages = await fetchMessages();
        return {
            statusCode: 200,
            body: JSON.stringify(messages),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            },
        };
    } catch (err) {
        return {
            statusCode: 500,
            body: JSON.stringify({ error: 'Server Error' }),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            },
        };
    }
};
