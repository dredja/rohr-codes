Testereignis Konfiguration:

1. **Name**: TestPutInDB <Dieser Name kann eigenständig ausgewählt werden>

2. **Ereignis-JSON**
```json
{
  "paketTyp": "IrgendeinInput",
  "name": "IrgendeinInput",
  "email": "IrgendeinInput",
  "message": "IrgendeinInput"
}
```
Die Werte wie IrgendeinInput können eigenständig definiert werden.