const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const { DynamoDBDocumentClient, PutCommand } = require("@aws-sdk/lib-dynamodb");

const ddbClient = new DynamoDBClient({ region: "us-east-1" });
const ddbDocClient = DynamoDBDocumentClient.from(ddbClient);

async function createMessage(requestId, event) {
    const now = new Date().toISOString();

    const params = {
        TableName: 'TABLENAME', // Hier unter TABLENAME den Tabellennamen der DynamoDB eingeben
        Item: {
            'messageId': requestId, // Hier die Items anpassen welche in die DB kommen wie man es will
            'paketTyp': event.paketTyp, // Hier die Items anpassen welche in die DB kommen wie man es will
            'name': event.name, // Hier die Items anpassen welche in die DB kommen wie man es will
            'email': event.email, // Hier die Items anpassen welche in die DB kommen wie man es will
            'message': event.message, // Hier die Items anpassen welche in die DB kommen wie man es will
        },
    };

    try {
        await ddbDocClient.send(new PutCommand(params));
        return {
            statusCode: 201,
            body: JSON.stringify({ message: 'Message created' }),
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        };
    } catch (err) {
        console.error(err);
        throw err;
    }
}

exports.handler = async (event, context) => {
    const requestId = context.awsRequestId;

    if (event.paketTyp && event.name && event.email && event.message) {
        try {
            const response = await createMessage(requestId, event);
            return response;
        } catch (err) {
            return {
                statusCode: 500,
                body: JSON.stringify({ error: 'Server Error' }),
                headers: {
                    'Access-Control-Allow-Origin': '*',
                },
            };
        }
    } else {
        return {
            statusCode: 400,
            body: JSON.stringify({ error: 'Bad Request' }),
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        };
    }
};
